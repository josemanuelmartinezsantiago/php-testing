<?php
/**
 * Created by PhpStorm.
 * User: jmartinez
 * Date: 27/03/17
 * Time: 19:26
 */

namespace Tests\AppBundle\Util;

use PHPUnit\Framework\TestCase;
use AppBundle\Util\Calculator;


class CalculatorTest extends TestCase
{
    public function testAdd()
    {
        $calc = new Calculator();
        $result = $calc->add(30, 12);

        // assert that your calculator added the numbers correctly!
        $this->assertEquals(42, $result);
    }
}
