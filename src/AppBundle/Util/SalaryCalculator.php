<?php
/**
 * Created by PhpStorm.
 * User: jmartinez
 * Date: 28/03/17
 * Time: 10:28
 */

namespace AppBundle\Util;

use Doctrine\Common\Persistence\ObjectManager;

class SalaryCalculator
{
    /**
     * @var ObjectManager
     */
    private $entityManager;

    public function __construct(ObjectManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function calculateTotalSalary($id)
    {
        $employeeRepository = $this->entityManager->getRepository('AppBundle:Employee');
        $employee = $employeeRepository->find($id);

        return $employee->getSalary() + $employee->getBonus();
    }
}