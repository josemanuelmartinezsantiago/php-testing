<?php
/**
 * Created by PhpStorm.
 * User: jmartinez
 * Date: 28/03/17
 * Time: 10:50
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Employee;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class EmployeeController extends Controller
{
    /**
     * Creates an employee
     *
     * @Route("/employee")
     */
    public function createAction()
    {
        $employee = new Employee();
        $employee->setBonus(10);
        $employee->setName('martinez');
        $employee->setSalary(100);
        $employee->setCreated(new \DateTime());

        $em = $this->getDoctrine()->getManager();

        $em->persist($employee);

        $em->flush();

        return new Response('Saved employee with id:'.$employee->getId());
    }

    /**
     * Deletes an employee
     *
     * @Route("/employee/{id}/delete")
     */
    public function deleteAction($id)
    {
        $employee = $this->getDoctrine()->getRepository('AppBundle:Employee')->find($id);

        if(!$employee) {
            throw $this->createNotFoundException(
                'No employee found for id '.$id
            );
        }

        $em = $this->getDoctrine()->getManager();

        $em->remove($employee);
        $em->flush();

        return new Response();
    }
}