#!/bin/bash

# We need to install dependencies only for Docker
[[ ! -e /.dockerenv ]] && exit 0

set -xe

# Install git (the php image doesn't have it) which is required by composer
apt-get update -yqq
apt-get install git -yqq
apt-get install zlib1g-dev -yqq
pecl install xdebug

cp ci/timezone.ini /usr/local/etc/php/conf.d/timezone.ini

# Install phpunit, the tool that we will use for testing
# not needed because we download phpunit from composer
# curl --location --output /usr/local/bin/phpunit https://phar.phpunit.de/phpunit.phar
# chmod +x /usr/local/bin/phpunit

# Install mysql driver
docker-php-ext-install pdo_mysql

docker-php-ext-enable xdebug

# install xdebug for code coverage
# docker-php-ext-install xdebug

# install zip
docker-php-ext-install zip

# Install composer
curl -sS https://getcomposer.org/installer | php

# Install all project dependencies
php composer.phar update

# if needed, we must configure our php.ini
# cp ci/my_php.ini /usr/local/etc/php/conf.d/test.ini